import { Component, OnInit } from '@angular/core';
import { LoanData, ColumnSetting } from '../ng-table/model';
import { SAMPLE_DATA_TEST } from '../../sample-data';

@Component({
  selector: 'app-loan',
  templateUrl: './loan.component.html',
  styleUrls: ['./loan.component.scss']
})
export class LoanComponent implements OnInit {

  loanConfig: ColumnSetting[] = [
    { key: 'name', header: 'Full Name' },
    { key: 'phone', header: 'Phone' },
    { key: 'email', header: 'Email' },
    { key: 'company', header: 'Company' },
    { key: 'date_entry', header: 'Data Entry' },
    { key: 'org_num', header: 'Org Number' },
    { key: 'address_1', header: 'Address 1' },
    { key: 'city', header: 'City' },
    { key: 'zip', header: 'Zip' },
    { key: 'geo', header: 'Geo' },
    { key: 'pan', header: 'PAN' },
    { key: 'pin', header: 'Pin' },
    { key: 'id', header: 'Id' },
    { key: 'status', header: 'Status' },
    { key: 'fee', header: 'Fee' },
    { key: 'guid', header: 'GUID' },
    { key: 'date_exit', header: 'Date_exit' },
    { key: 'date_first', header: 'Date_first' },
    { key: 'date_recent', header: 'Date_recent' },
    { key: 'url', header: 'URI' }
];

loanInformation: LoanData[] = [];

  constructor() { }

  ngOnInit(): void {
    this.loanInformation  = SAMPLE_DATA_TEST as LoanData[];
  }

}
