import { Component, OnInit } from '@angular/core';
import { ColumnSetting } from '../ng-table/model';

@Component({
  selector: 'app-person',
  templateUrl: './person.component.html',
  styleUrls: ['./person.component.scss']
})
export class PersonComponent implements OnInit {

  personalConfig: ColumnSetting[] = [
    { key: 'id', header: 'Id' },
    { key: 'name', header: 'Full Name' },
    { key: 'job', header: 'Job' },
    { key: 'year_joined', header: 'Year Joined' }
  ];

  persons = [
    {
      id: 152,
      name: 'Virgil I. Grissom',
      job: 'Astronaut',
      year_joined: 1959
  },
  {
      id: 153,
      name: 'John H. Glenn, Jr.',
      job: 'Astronaut',
      year_joined: 1959
  }
];

PERSONAL_CONFIG_2: ColumnSetting[] = [
  { key: 'name', header: 'Full Name' },
  { key: 'employer', header: 'Employer' },
  { key: 'client', header: 'Client' },
  { key: 'vendor', header: 'Vendor' },
];

PERSON_2 =  [
  {
    name: 'Ashok Masuram',
    employer: 'Gantec',
    client: 'FreddieMac',
    vendor: 'Hexaware'
  },
  {
    name: 'Mohan Reddy Katha',
    employer: 'Hexaware',
    client: 'FreddieMac',
    vendor: 'Hexaware'
  },
  {
    name: 'Srikanth',
    employer: 'Capco',
    client: 'FreddieMac',
    vendor: 'Capco'
  }
];

  constructor() { }

  ngOnInit() {
  }

}
