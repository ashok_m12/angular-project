import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NgTableComponent } from './ng-table.component';
import { HttpClientModule } from '@angular/common/http';
import { PagerService } from '../pager-service';
import { RestService } from '../rest.service';

export const MOCK_DATA = [
  {
    name: 'Ashok Masuram',
    employer: 'Gantec',
    client: 'FreddieMac',
    vendor: 'Hexaware'
  },
  {
    name: 'Mohan Katha',
    employer: 'Hexaware',
    client: 'FreddieMac',
    vendor: 'Hexaware'
  }
];
describe('NgTableComponent', () => {
  let component: NgTableComponent;
  let fixture: ComponentFixture<NgTableComponent>;
  let pagerService: PagerService;
  let restService: RestService;
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NgTableComponent ],
      imports: [
        HttpClientModule
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NgTableComponent);
    component = fixture.componentInstance;
    pagerService = fixture.debugElement.injector.get(PagerService);
    restService = fixture.debugElement.injector.get(RestService);

    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  describe('Table Display by providing data', () => {
  let spySetPage;
  beforeEach(() => {
    component.data = MOCK_DATA;
    spySetPage = spyOn(component, 'setPage');
    spyOn(pagerService, 'getPager').and.returnValue(
       { totalItems: 2, currentPage: 1, pageSize: 1, totalPages: 1,
        startPage: 1, endPage: 1, startIndex: 1, endIndex: 1, pages: []
      });
    fixture.detectChanges();
    component.ngOnInit();
  });

  it('should call setPage method', () => {
    expect(spySetPage).toHaveBeenCalledWith(1);
  });
  it('length of data processing ', () => {
    expect(component.allItems.length).toBe(MOCK_DATA.length);
  });
});




});
