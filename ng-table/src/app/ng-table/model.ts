export interface LoanData {
name?: string;
phone?: string;
email?: string;
company?: string;
date_entry?: string;
org_num?: string;
address_1?: string;
city?: string;
zip?: number;
geo?: string;
pan?: string;
pin?: number;
id?: number;
status?: string;
fee?: string;
guid?: string;
date_exit?: string;
date_first?: string;
date_recent?: string;
url?: string;
}

export class ColumnSetting {
key: string;
header: string;
}

