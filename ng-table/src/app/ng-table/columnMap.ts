
export class ColumnMap {
    key: string;
    private _HEADER: string;

    constructor(settings) {
        this.key = settings.key;
        this.header = settings.header;
    }
    set header(setting: string) {
        this._HEADER = setting ?
            setting : this.header;
    }
    get header() {
        return this._HEADER;
    }

    access = function(object: any) {
        if (object[this.key]) {
            return this.key;
        }
        return this.key;
    };
}
