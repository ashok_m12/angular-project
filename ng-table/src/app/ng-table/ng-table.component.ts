import { Component, OnInit, Input, OnDestroy, OnChanges } from '@angular/core';
import { ColumnMap } from './columnMap';
import { PagerService } from '../pager-service';
import { RestService } from '../rest.service';

@Component({
  selector: 'app-ng-table',
  templateUrl: './ng-table.component.html',
  styleUrls: ['./ng-table.component.scss']
})
export class NgTableComponent implements OnInit, OnChanges, OnDestroy {

  @Input() data: any[] = [];
  @Input() settings: any;
  @Input() tableHeader: string;
  columnMaps: ColumnMap[] = [];
  errorMessage: string;

  // array of all items to be paged
   allItems: any[] = [];

  // pager object
  pager: any = {};

  // paged items
  pagedItems: any[];

  constructor(private pagerService: PagerService,
              private restService: RestService) { }

  ngOnInit() {

    this.allItems = this.data;
    // initialize to page 1
    this.setPage(1);
  }

  setPage(page: number) {
    // get pager object from service
    this.pager = this.pagerService.getPager(this.allItems.length, page);
    // get current page of items
    this.pagedItems = this.allItems.slice(this.pager.startIndex, this.pager.endIndex + 1);
  }

  checkRow(event, record) {
    if (event.target.checked) {
      console.log('Selected Record ==>', record);
    } else {
      console.log('UnSelected Record ==>', record);
    }
  }


  postData(event, record) {
    console.dir(record);
    this.restService.postLoanData(record).subscribe(data => {
      // Success: Data can be shown or transformed
      console.log('SUCCESS');
    },
      error => {
        // Customize the backend error message
        this.errorMessage = error;
      }
    );
  }

  ngOnChanges() {
    console.log('ngOnchanges-->');
    if (this.settings) {
      this.columnMaps = this.settings
        .map(col => new ColumnMap(col));
    } else {
      this.columnMaps = Object.keys(this.data[0]).map(key => {
        return new ColumnMap({ primaryKey: key });
      });
    }
  }

  ngOnDestroy(): void {
  }

}
