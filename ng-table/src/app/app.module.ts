import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './AppRoutingModule';
import { AppComponent } from './app.component';
import { NgTableComponent } from './ng-table/ng-table.component';
import { HttpClientModule } from '@angular/common/http';
import { LoanComponent } from './loan/loan.component';
import { PersonComponent } from './person/person.component';

@NgModule({
  declarations: [
    AppComponent,
    NgTableComponent,
    LoanComponent,
    PersonComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
