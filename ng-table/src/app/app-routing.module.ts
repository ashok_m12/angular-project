import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AppComponent } from './app.component';
import { LoanComponent } from './loan/loan.component';
import { PersonComponent } from './person/person.component';


export const routes: Routes = [

    {
      path: 'loan', // child route path
      component: LoanComponent // child route component that the router renders
    },
    {
      path: 'person',
      component: PersonComponent // another child route component that the router renders
    },
    { path: '',   redirectTo: '/loan', pathMatch: 'full' },

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
